import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Chart } from './chart.model';

@Injectable()
export class ChartService {
    private host: string = environment.host;

    constructor(private http: HttpClient) { }

    public get(): Observable<Chart[]> {
        return this.http.get(this.host + '/v1/chart')
            .map((response: any) => {
                return response;
            }).catch((response: any) => {
                return Observable.throw(response.error.message || 'Server error');
            });
    }
}

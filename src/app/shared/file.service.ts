import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class FileService {
    private host: string = environment.host;

    constructor(private http: HttpClient) { }

    public upload(file: string): Observable<boolean> {
        return this.http.post(this.host + '/v1/upload', {data: file})
            .map((response: any) => {
                return true;
            }).catch((response: any) => {
                return Observable.throw(response.error.message || 'Server error');
            });
    }
}

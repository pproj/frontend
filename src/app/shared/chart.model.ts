export class Chart {
    public ticket: number;
    public open_time: number;
    public type: string;
    public comment: string;
    public profit: number;
}
import { ChartService } from './shared/chart.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ChartService]
})
export class AppComponent {
  public data: any = [];

  constructor(private chartService: ChartService){
    this.requestData();
  }

  public requestData() {
    this.chartService.get().subscribe(data => {
      this.data = data;
    });
  }
}

import { Component, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UploadComponent } from '../upload/upload.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  @Output() uploadClose = new EventEmitter();

  constructor(private dialog: MatDialog){ }

  public uploadDialog(): void {
    const dialogRef = this.dialog.open(UploadComponent, {
      width: '550px'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.uploadClose.emit(true);
    });
  }
}

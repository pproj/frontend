import { Chart as ChartModel } from './../shared/chart.model';
import { Component, OnChanges, Input } from '@angular/core';
import { StockChart } from 'angular-highcharts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnChanges {
  @Input() data: any = [];
  public chart: StockChart;

  ngOnChanges(value: any) {
    const data: ChartModel[] = value.data.currentValue;
    if (!data.length) {
      return false;
    }

    this.updateChartData(data);
  }

  private updateChartData(data: ChartModel[]) {
    const seriesData = [];

    this.data.forEach(item => {
      seriesData.push([parseInt(item.open_time, 10), parseFloat(item.profit)]);
    });

    this.chart = new StockChart({
      rangeSelector: {
        selected: 2
      },
      title: {
        text: 'Баланс'
      },
      tooltip: {
        valueDecimals: 3
      },
      series: [{
        type: 'column',
        name: 'Баланс',
        data: seriesData
      }]
    });
  }
}

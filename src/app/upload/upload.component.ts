import { FileService } from './../shared/file.service';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
  providers: [FileService]
})
export class UploadComponent {
  public uploadForm = this.fb.group({
    file: [null, Validators.required]
  });

  constructor(
    private fb: FormBuilder, private errorBar: MatSnackBar,
    private fileService: FileService, private dialogRef: MatDialogRef<UploadComponent>
  ) { }

  public onSubmit() {
    this.fileService.upload(this.uploadForm.value.file).subscribe(
      result => {
        this.errorBar.open('Файл загружен', '', {
          duration: 2000
        });
        this.dialogRef.close();
      },
      error => {
        this.errorBar.open(error, 'Понял');
      }
    );
  }

  public onFileChange(event) {
    const reader = new FileReader();

    this.uploadForm.patchValue({
      file: undefined
    });

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      const filename = file.name.split('.');

      if (filename.length < 2 || filename[1] !== 'html') {
        this.errorBar.open('Неверный формат файла, допустимы только html файлы', 'Понял');
        return false;
      }

      reader.readAsDataURL(file);
      reader.onload = () => {
        this.uploadForm.patchValue({
          file: reader.result
        });
      };
    }
  }
}